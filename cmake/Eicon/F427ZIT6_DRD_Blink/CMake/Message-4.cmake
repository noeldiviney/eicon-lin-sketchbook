MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   add_custom_command(TARGET      ${PROJECT_NAME}.elf POST_BUILD")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}            COMMAND               ${CMAKE_OBJCOPY} ARGS -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}            COMMAND               ${CMAKE_OBJCOPY} ARGS -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   COMMENT                        Building ${HEX_FILE} ")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   COMMENT                        Building ${BIN_FILE} ")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   All done ----------------------------------------------------------------------------------------------------")
#---------------  All Done  ---------------------------------------------------------------------------------------------------#
